package com.enlighten.challenge.service;

import com.enlighten.challenge.model.Alert;
import com.enlighten.challenge.model.SatelliteLog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SatelliteLogServiceTest {

	@Test
	void serviceTest() {

		SatelliteLogService service = new SatelliteLogService();

		SatelliteLog testB1Log = new SatelliteLog("1001",10,100, new Date(),1.1,"BATT");
		SatelliteLog testB2Log = new SatelliteLog("1001",10,100, new Date(),1.1,"BATT");
		SatelliteLog testB3Log = new SatelliteLog("1001",10,100, new Date(),1.1,"BATT");

		SatelliteLog testT1Log = new SatelliteLog("1001",10,100, new Date(),101.1,"TSTAT");
		SatelliteLog testT2Log = new SatelliteLog("1001",10,100, new Date(),101.1,"TSTAT");
		SatelliteLog testT3Log = new SatelliteLog("1001",10,100, new Date(),101.1,"TSTAT");

		service.addLog(testB1Log);
		service.addLog(testB2Log);
		service.addLog(testB3Log);

		service.addLog(testT1Log);
		service.addLog(testT2Log);
		service.addLog(testT3Log);

		assertEquals(1, service.getTstatLogs().size());
		assertEquals(3, service.getTstatLogs().get("1001").size());

		assertEquals(1, service.getBattLogs().size());
		assertEquals(3, service.getBattLogs().get("1001").size());

		String alertsJson = service.getAlerts();

		Gson gson = new Gson();

		List<Alert> alerts = gson.fromJson(alertsJson, new TypeToken<List<Alert>>(){}.getType());

		assertEquals(2, alerts.size());
	}
}
