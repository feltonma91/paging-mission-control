package com.enlighten.challenge.model;

import java.util.Date;

public class SatelliteLog {

	private String id;
	private int redLow;
	private int redHigh;
	private Date timeStamp;
	private double value;
	private String component;

	public SatelliteLog(String id, int redLow, int redHigh, Date timeStamp, double value, String component) {
		this.id = id;
		this.redLow = redLow;
		this.redHigh = redHigh;
		this.timeStamp = timeStamp;
		this.value = value;
		this.component = component;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getRedLow() {
		return redLow;
	}

	public void setRedLow(int redLow) {
		this.redLow = redLow;
	}

	public int getRedHigh() {
		return redHigh;
	}

	public void setRedHigh(int redHigh) {
		this.redHigh = redHigh;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}
}
