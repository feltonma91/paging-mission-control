package com.enlighten.challenge.model;

import java.util.Date;

public class Alert {

	private String satelliteId;
	private String severity;
	private String component;
	private Date date;

	public Alert(String satelliteId, String severity, String component, Date date) {
		this.satelliteId = satelliteId;
		this.severity = severity;
		this.component = component;
		this.date = date;
	}

	public String getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(String satelliteId) {
		this.satelliteId = satelliteId;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
