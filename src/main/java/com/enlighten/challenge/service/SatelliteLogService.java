package com.enlighten.challenge.service;

import com.enlighten.challenge.model.Alert;
import com.enlighten.challenge.model.SatelliteLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import org.apache.commons.lang.time.DateUtils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SatelliteLogService {

	// Constants
	private static final String BATT = "BATT";
	private static final String TSTAT = "TSTAT";
	private static final String RED_HIGH = "RED HIGH";
	private static final String RED_LOW = "RED LOW";

	// Logs
	private final HashMap<String, List<SatelliteLog>> battLogs =  new HashMap<>();
	private final HashMap<String, List<SatelliteLog>> tstatLogs =  new HashMap<>();

	// Alerts
	private final List<Alert> alerts = new ArrayList<>();

	public void addLog(SatelliteLog satelliteLog) {
		switch (satelliteLog.getComponent()){
			case TSTAT:
				// update temperature log Map
				updateLogMap(satelliteLog, this.tstatLogs);
				break;
			case BATT:
				// update battery log Map
				updateLogMap(satelliteLog, this.battLogs);
				break;
			default:
				// Shouldn't make it here
		}
	}

	public String getAlerts(){
		calculateTAlerts();
		calculateBAlerts();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonArray alertArray = new JsonArray();
		for( Alert alert : alerts){
			alertArray.add(gson.toJsonTree(alert));
		}
		return gson.toJson(alertArray);
	}

	private void updateLogMap(SatelliteLog satelliteLog, HashMap<String, List<SatelliteLog>> logs) {

		// create log list if key is not found in map
		if(!logs.containsKey(satelliteLog.getId())){
			List<SatelliteLog> list = new ArrayList<>();
			list.add(satelliteLog);
			logs.put(satelliteLog.getId(), list);
		} // key found in map add log to list
		else {
			logs.get(satelliteLog.getId()).add(satelliteLog);
		}
	}

	private void calculateTAlerts(){
		tstatLogs.forEach((id, logList) -> {
			int left = 0;
			int right = 0;
			int warningCounter = 0;
			while (left < logList.size() && right < logList.size()){
				// Set threshold
				Date threshold = DateUtils.addMinutes(logList.get(left).getTimeStamp(), 5);
				// grab the current log
				SatelliteLog current = logList.get(right);
				if( current.getTimeStamp().before(threshold) ){
					// check current value against red high value
					if(current.getValue() > current.getRedHigh()){
						warningCounter++;
					}
					if( warningCounter == 3) {

						// Violation found in the current threshold
						Alert alert = new Alert(current.getId(),RED_HIGH,TSTAT,current.getTimeStamp());
						alerts.add(alert);
						// update threshold by sliding the window
						left++;
						// reset right
						right = left;
						// reset counter
						warningCounter = 0;
					}

					// No violation found within the 5 min interval check next log
					right++;

				} else { // current timestamp exceeds the 5 minute threshold
					// update threshold by sliding the window
					left++;
					// reset right
					right = left;
					// reset counter
					warningCounter = 0;
				}
			}
		});
	}

	private void calculateBAlerts(){
		battLogs.forEach((id, logList) -> {
			int left = 0;
			int right = 0;
			int warningCounter = 0;
			while (left < logList.size() && right < logList.size()){
				// Set threshold
				Date threshold = DateUtils.addMinutes(logList.get(left).getTimeStamp(), 5);
				// grab the current log
				SatelliteLog current = logList.get(right);
				if( current.getTimeStamp().before(threshold) ){
					// check current value against red low value
					if(current.getValue() < current.getRedLow()){
						warningCounter++;
					}
					if( warningCounter == 3) {

						// Violation found in the current threshold
						Alert alert = new Alert(current.getId(),RED_LOW,BATT,current.getTimeStamp());
						alerts.add(alert);
						// update threshold by sliding the window
						left++;
						// reset right
						right = left;
						// reset counter
						warningCounter = 0;
					}

					// No violation found within the 5 min interval yet
					right++;

				} else { // current timestamp exceeds the 5 minute threshold
					// update threshold by sliding the window
					left++;
					// reset right
					right = left;
					// reset counter
					warningCounter = 0;
				}
			}
		});
	}

	HashMap<String, List<SatelliteLog>> getBattLogs() {
		return battLogs;
	}

	HashMap<String, List<SatelliteLog>> getTstatLogs() {
		return tstatLogs;
	}
}
