package com.enlighten.challenge;

import com.enlighten.challenge.model.SatelliteLog;
import com.enlighten.challenge.service.SatelliteLogService;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Scanner;

public class Solution {


	public static void main(String[] args) {
		if( args.length < 1) {
			System.out.println("Please pass a file as an argument");
			System.exit(-1);
		}

		File inFile = new File(args[0]);
		SatelliteLogService service = new SatelliteLogService();
		try {
			Scanner fileReader = new Scanner(inFile);
			while (fileReader.hasNextLine()){
				// Read in file
				String line = fileReader.nextLine();
				// Split line into log parts
				String[] logParts = line.split("\\|");

				// Create satelliteLog
				Date timestamp = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS").parse(logParts[0]);
				String id =  logParts[1];
				int redHigh = Integer.parseInt(logParts[2]);
				String component = logParts[7];
				int redLow = Integer.parseInt(logParts[5]);
				double value = Double.parseDouble(logParts[6]);

				SatelliteLog satelliteLog = new SatelliteLog(id,redLow,redHigh,timestamp,value,component);
				service.addLog(satelliteLog);
			}
			// Display results
			System.out.println(service.getAlerts());

		} catch (FileNotFoundException | ParseException e){
			System.out.println("Unable to read file\n"+e.getMessage());
		}
	}
}
